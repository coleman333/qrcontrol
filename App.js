import React from 'react';
import { AppRegistry, Dimensions, StyleSheet, Text, View } from 'react-native';
import { Provider } from "react-redux";
import configureStore from './configureStore';
const store = configureStore();
import Navigator from "./containers/Navigator";
import MainMenu from "./containers/MainMenu";
import QrScanner from "./containers/QrScanner"
import { CHANGE_DIMENSIONS } from './actions/dimensionAction';

export default class App extends React.Component {
    constructor(props) {
        super(props);

        // let dim = Dimensions.get('screen');
        // store.dispatch(CHANGE_DIMENSIONS({
        //     height: dim.height,
        //     width: dim.width,
        //     orientation: (dim.width >= dim.height) ? 'landscape' : 'portrait'
        // }));

        Dimensions.addEventListener('change', () => {
            let dim = Dimensions.get('screen');
            store.dispatch(CHANGE_DIMENSIONS({
                height: dim.height,
                width: dim.width,
                orientation: (dim.width >= dim.height) ? 'landscape' : 'portrait'
            }));
        });
    }

    render() {
        return (
            <Provider store={store}>
                {/*<CardNavigator/>*/}
                <Navigator/>
            </Provider>
        );
    }
}

AppRegistry.registerComponent('App', () => App);