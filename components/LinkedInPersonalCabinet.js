import React from 'react';
import {StyleSheet, Text, Button, View, Animated, ScrollView, KeyboardAvoidingView} from 'react-native';
import { connect } from "react-redux";
import userAction from "../actions/userAction";
import {bindActionCreators} from "redux";
import * as shape from 'd3-shape'
import { FormLabel, FormInput, FormValidationMessage } from 'react-native-elements'
import _ from 'lodash';

import { BarChart, Grid, PieChart ,StackedBarChart,AreaChart} from 'react-native-svg-charts'
import Icon from "react-native-vector-icons/FontAwesome";
class LinkedInPersonalCabinet extends React.Component {

    state={
        heightOfElementWhenKeyboardOn:'100%',
        newUserName: "",

    };
    growAnimation = new Animated.Value(0);
    componentWillUnmount(){
        this.interval && clearInterval(this.interval);
    }
    componentDidMount(){
        this.interval=setInterval(()=>{
            console.log(123,this.data3[3].bananas)
            this.data3[3].bananas-=100;
            this.data3[2].bananas+=100;
            this.data[4] +=50;
            this.data[5]+=30;
            this.data[4]-=30;
            this.data[6]+=30;
            this.data2[4]+=5;
            this.data2[6]-=5;
            this.forceUpdate()
        },2000)

        Animated.timing(
            this.growAnimation,
            {
                toValue: 1,
                duration: 2000,
                // easing: Easing.linear,
                useNativeDriver: true
            }
        ).start();
        this.setState({disAnimate:false})
    }
    data3=[
        {
            month: new Date(2015, 0, 1),
            apples: 3840,
            bananas: 1920,
            cherries: 960,
            dates: 400,
            oranges: 400,
        },
        {
            month: new Date(2015, 1, 1),
            apples: 1600,
            bananas: 1440,
            cherries: 960,
            dates: 400,
        },
        {
            month: new Date(2015, 2, 1),
            apples: 640,
            bananas: 960,
            cherries: 3640,
            dates: 400,
        },
        {
            month: new Date(2015, 3, 1),
            apples: 3320,
            bananas: 480,
            cherries: 640,
            dates: 400,
        },
    ];

    data   = [ 50, 10, 40, 95, -4, -24, null, 85, undefined, 0, 35, 53, -53, 24, 50, -20, -80 ];

    data2 = [ 50, 10, 40, 95, -4, -24, 85, 91, 35, 53, -53, 24, 50, -20, -80 ];

    render() {
        const data3 = this.data3;
        const data2 = this.data2;
        const data = this.data;

        const GA = this.growAnimation.interpolate({
            inputRange: [0, 1],
            outputRange: [-500, 0]
        });
        // const fill = 'rgb(134, 65, 244)';
        const fill = 'purple';

        const randomColor = () => ('#' + (Math.random() * 0xFFFFFF << 0).toString(16) + '000000').slice(0, 7);

        const pieData = data
            .filter(value => value > 0)
            .map((value, index) => ({
                value,
                svg: {
                    fill: randomColor(),
                    onPress: () => console.log('press', index),

                    // animationDuration:300
                },
                key: `pie-${index}`,
            }));
        const colors = [ '#7b4173', '#a55194', '#ce6dbd', '#de9ed6' ];
        const keys   = [ 'apples', 'bananas', 'cherries', 'dates' ];

console.log('linkedin')
        return (
            <ScrollView>

                <View style={{backgroundColor:'black'}}>
                    <AreaChart
                        style={{ height: 200 ,backgroundColor:'black' }}
                        data={ data }
                        animate={true}
                        animationDuration={400}
                        contentInset={{ top: 30, bottom: 30 }}
                        curve={ shape.curveNatural }
                        svg={{ fill: 'rgba(134, 65, 244, 0.8)' }}
                    >
                        <Grid/>
                    </AreaChart>
                    <BarChart
                        style={{ height: 300, width:500, backgroundColor:'black'}}
                        // animate:{true}
                        // animationDuration:{500}
                        // ticks:{9}
                        yMin={20}
                        // horizontal={true}
                        numberOfTicks={20}
                        data={ data2 }
                        animate={true}
                        showGrid={true}
                        svg={{ fill }}
                        contentInset={{ top: 30, bottom: 30 }}
                    >
                        <Grid/>
                    </BarChart>
                    <View  style={{height:20,width:'100%',backgroundColor:'black', borderWidth: 1,
                        borderColor: 'white',borderTopLeftRadius:25,borderRadius:25 }}>
                        <Text style={{color:'white',display: "flex", justifyContent: "center",alignItems: 'center'
                        }}>linked in likes statistics</Text>
                        <Icon name="like" size={30} color={'white'} style={{marginLeft: 10}}/>
                    </View>
                    <PieChart
                        style={ { height: 200 ,backgroundColor:'black'} }
                        animate={true}
                        data={ pieData }
                    />
                    <StackedBarChart
                        style={ { height: 200, backgroundColor:'black'} }
                        animate={true}
                        keys={ keys }
                        colors={ colors }
                        data={ data3 }
                        showGrid={ false }
                        contentInset={ { top: 30, bottom: 30 } }
                    />
                </View>
            </ScrollView>
        );
    }
}



const mapStateToProps = (state) => {
    return {
        user: state.userReducer.user

    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        userAction: bindActionCreators(userAction, dispatch),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(LinkedInPersonalCabinet);
