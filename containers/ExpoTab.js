import React from 'react';
import {View, StatusBar, StyleSheet, Dimensions, Text, Image, TouchableOpacity} from 'react-native';
import { TabViewAnimated,TabView, TabBar, SceneMap } from 'react-native-tab-view';
import PersonalCabinetStatistic from "./PersonalCabinetStatistic"; // 0.0.67
import MenuToScanGood from "./MenuToScanGood"; // 0.0.67
import FaceBookPersonalCabinet from "../components/FaceBookPersonalCabinet"; // 0.0.67
import GooglePlusPersonalCabinet from "../components/GooglePlusPersonalCabinet"; // 0.0.67
import LinkedInPersonalCabinet from "../components/LinkedInPersonalCabinet"; // 0.0.67
import PinterestPersonalCabinet from "../components/PinterestPersonalCabinet"; // 0.0.67
import QrCodePersonalCabinet from "../components/QrCodePersonalCabinet"; // 0.0.67
import InstagramPersonalCabinet from "../components/InstagramPersonalCabinet"; // 0.0.67
import TwitterPersonalCabinet from "../components/TwitterPersonalCabinet"; // 0.0.67
import facebook from '../images/Facebook.png';
import Icon from "react-native-vector-icons/FontAwesome";



export default class TabViewExample extends React.Component {
    state = {
        index: 0,
        routes: [
            // { key: 'first', title: `<!--<View><Text>First 🐴</Text><Image source={require(\'../images/pinterest.png\')} ></Image></View>-->`},
            { key: 'first' },
            { key: 'second' },
            { key: 'third' },
            { key: 'fourth' },
            { key: 'fifth' },
            { key: 'sixth' },

            // { key: 'seventh' },
        ],
    };

    // _renderScene = SceneMap({
    //     first: FirstRoute,
    //     second: SecondRoute,
    // });

    // renderTabBar={(props)=>{
    //     return( <TabBar
    //     {...props}
    //     indicatorStyle={{ backgroundColor: 'pink' }}
    //     />)
    // }
    // }

    scenes={
            // first: QrCodePersonalCabinet,
            first: PinterestPersonalCabinet,
            second: FaceBookPersonalCabinet,
            third: LinkedInPersonalCabinet,
            fourth: InstagramPersonalCabinet,
            fifth: TwitterPersonalCabinet,
            sixth: GooglePlusPersonalCabinet,

            // seventh: PinterestPersonalCabinet,
        };

    render() {
        const size = 50;
        const notificationsSize = 10;
        const notificationsColor = 'yellow';
        const iconColor = '#424242';
        let firstNot = true;
        return (
            <TabView
                navigationState={this.state}
                // renderScene={SceneMap({
                //     // first: QrCodePersonalCabinet,
                //     first: PinterestPersonalCabinet,
                //     second: FaceBookPersonalCabinet,
                //     third: LinkedInPersonalCabinet,
                //     // fourth: InstagramPersonalCabinet,
                //     // fifth: TwitterPersonalCabinet,
                //     // sixth: GooglePlusPersonalCabinet,
                //
                //     // seventh: PinterestPersonalCabinet,
                // })}
                renderScene={({route}={}) => {
                    if (this.state.index !== this.state.routes.indexOf(route)) {
                        return <View/>;
                    }

                    return React.createElement(this.scenes[route.key])
                }}
                renderTabBar={props =>
                    <TabBar
                        {...props}
                        renderBadge={props=>{
                            const {route:{key}={}}= props;
                            switch (key) {
                                case 'first':{
                                    {return firstNot &&<Icon name="bell" size={notificationsSize}  color={notificationsColor}
                                                 style={{marginRight: 2}} />}
                                }
                                case 'second':{
                                    return <Icon name="bell" size={notificationsSize} color={notificationsColor} style={{marginRight: 2}}/>
                                }
                                case 'third':{
                                    return <Icon name="bell" size={notificationsSize} color={notificationsColor} style={{marginRight: 2}} />
                                }
                                case 'fourth':{
                                    return <Icon name="bell" size={notificationsSize} color={notificationsColor} style={{marginRight: 2}} />
                                }
                                case 'fifth':{
                                    return <Icon name="bell" size={notificationsSize} color={notificationsColor} style={{marginRight: 2}} />
                                }
                                case 'sixth':{
                                    return <Icon name="bell" size={notificationsSize} color={notificationsColor} style={{marginRight: 2}} />
                                }

                                // case 'seventh':{
                                //     return <Icon name="bell" size={notificationsSize} color={notificationsColor} style={{marginRight: 2}} />
                                // }
                                default:
                                    return <Icon name="bell" size={notificationsSize} color={notificationsColor} style={{marginRight: 2}} />
                            }
                        }}

                        // renderLabel={props=>{
                        //     const {route:{key}={}}=props;
                        //     switch (key) {
                        //         case 'first':{
                        //             return  <Text>qrcode</Text>
                        //         }
                        //         case 'second':{
                        //             return <Text>facebook</Text>
                        //         }
                        //         case 'third':{
                        //             return <Text>linkedin</Text>
                        //         }
                        //         case 'fourth':{
                        //             return <Text>instagram</Text>
                        //         }
                        //         case 'fifth':{
                        //             return <Text>twitter</Text>
                        //         }
                        //         case 'sixth':{
                        //             return <Text>google+</Text>
                        //         }
                        //         default:
                        //             return <Text>pinterest</Text>
                        //     }
                        // }}
                        renderIcon={props=>{
                            const {route:{key}={}}=props;

                            switch (key) {
                                case 'first':{
                                    // return <Icon name="qrcode" size={size}  color={iconColor} style={{marginRight: 2}} />
                                    return <Icon name="pinterest-square" size={size} color={iconColor} style={{marginRight: 2}} />
                                }
                                case 'second':{
                                    return <Icon name="facebook-square" size={size} color={iconColor} style={{marginRight: 2}}/>
                                }
                                case 'third':{
                                    return <Icon name="linkedin-square" size={size} color={iconColor} style={{marginRight: 2}} />
                                }
                                case 'fourth':{
                                    return <Icon name="instagram" size={size} color={iconColor} style={{marginRight: 2}} />
                                }
                                case 'fifth':{
                                    return <Icon name="twitter-square" size={size} color={iconColor} style={{marginRight: 2}} />
                                }
                                case 'sixth':{
                                    return <Icon name="google-plus-square" size={size} color={iconColor} style={{marginRight: 2}} />
                                }

                                // case 'seventh':{
                                //     return <Icon name="pinterest-square" size={size} color={iconColor} style={{marginRight: 2}} />
                                // }
                                default:
                                    // return <Icon name="pinterest-square" size={size} color={iconColor} style={{marginRight: 2}} />
                            }

                        }}
                    />
                }
                onIndexChange={index => this.setState({ index })}
                initialLayout={{ width: Dimensions.get('window').width }}
            />
        );
    }
}
