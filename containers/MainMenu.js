import React from 'react';
import {
    StyleSheet,
    Text,
    Button,
    View,
    TextInput,
    TouchableOpacity,
    DrawerLayoutAndroid,
    Platform,
    ScrollView,
    KeyboardAvoidingView,
    Animated,
    Keyboard,
    Dimensions,
    Image,
    Easing,
    Modal,
    TouchableHighlight,
    TouchableWithoutFeedback,
} from 'react-native';
//import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {connect} from "react-redux";
import userAction from '../actions/userAction';
import {bindActionCreators} from 'redux';
import {FormLabel, FormInput, FormValidationMessage} from 'react-native-elements'
import _ from 'lodash';
import LinkedInModal from 'react-native-linkedin'
import NotificationWrapper from './NotificationWrapper';
import PersonalCabinetStatistic from './PersonalCabinetStatistic';
import RNTesseractOcr from 'react-native-tesseract-ocr';

import Icon from 'react-native-vector-icons/FontAwesome';


import {StackActions, NavigationActions} from 'react-navigation';
// var FBLoginButton = require('./FBLoginButton');
// import styles, { IMAGE_HEIGHT, IMAGE_HEIGHT_SMALL} from './styles';
const IMAGE_HEIGHT = 408;
const IMAGE_HEIGHT_SMALL = 24;
import logo from '../images/Facebook.png';
import InstagramLogin from 'react-native-instagram-login';
import Cookie from 'react-native-cookie'
import RNTextDetector from "react-native-text-detector";

import {Ionicons} from "@expo/vector-icons";
// const { LoginManager, LoginButton } = require('react-native-fbsdk');
// import pushNotificationMethod from '../utils/pushNotifications';
// const {token} = pushNotificationMethod;

class MainMenu extends React.Component {
    constructor(props) {
        super(props);
        this.myRef = React.createRef();
        this.y_translate = new Animated.Value(0);

    }


    authorization() {
        // this.props.userAction.getTestUsers();
        // const { navigate } = this.props.navigation;

        // console.log(navigate)
        // navigate('menuToScanGood');


        const resetAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({routeName: 'QrScanner'}, {page: 'auth'}),
            ],
        });
        this.props.navigation.dispatch(resetAction);


        // navigate('QrScanner');

        // this.props.userAction.login(name);
    }


    componentWillMount() {
        this.animatedValue1 = new Animated.Value(1);

        if (this.props.navigation.state.params) {
            const {page} = this.props.navigation.state.params;
            if (page === "unAuthorized") {
                alert('Вы не зарегистрированы')
            }
        }
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));

        this.setState({launchAnimation: 'inActive'})

        // this.onChange=_.debounce(this.onChange.bind(this), 1000);
    }

    spinValue = new Animated.Value(0);
    _animValue2 = new Animated.Value(0);

    componentDidMount() {

        Animated.loop(
            Animated.timing(
                this.spinValue,
                {
                    toValue: 1,
                    duration: 3000,
                    // easing: Easing.linear,
                    useNativeDriver: true
                }
            )
        ).start();

    }

    state = {
        toScroll: false,
        keyboardHeight: 0,
        normalHeight: 0,
        shortHeight: 0,
        heightOfElementWhenKeyboardOn: '100%',
        item: 1,
        // launchAnimation: 'active',
        modalVisible: false,
        facebookName: '',
        isntaToken: '',
        disAnimate: true,
        menu_expanded: false
    };


    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow(e) {
        this.setState({toScroll: true});
        this.setState({shortHeight: Dimensions.get('window').height - e.endCoordinates.height});
        this.setState({normalHeight: Dimensions.get('window').height});
        this.setState({keyboardHeight: e.endCoordinates.height});
        this.setState({heightOfElementWhenKeyboardOn: this.state.shortHeight});
    }

    onScrollTo(item) {
        console.log('in method on scroll to');
        console.log(this.state.heightOfElementWhenKeyboardOn);
        this.myRef.current.scrollTo({y: this.state.item * 100, animated: true});
    }


    _keyboardDidHide() {
        this.setState({toScroll: false});
        this.setState({heightOfElementWhenKeyboardOn: this.state.normalHeight});

    }

    checkEmail() {
    }

    onChange(ev) {
        // console.log('refs', this.emailInput);
        console.log(ev)
    }

    checkName(ev) {
        // this.props.userAction.checkName();
        // console.log('refs', this.refs);
        console.log('this is new name value', ev.nativeEvent.text)

        // _debounce()

    }

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    authLinkedIn() {
        this.props.userAction.authLinkedIn();
    }

    authFacebook() {
        this.props.userAction.authFB();
    }

    authTwitter() {
        this.props.userAction.authTW();
    }

    authGoogle() {
        this.props.userAction.authGoogle();
    }

    authInstagram() {
        this.props.userAction.authGoogle();
    }

    authPinterest() {
        this.props.userAction.authGoogle();
    }

    // FBLogin(){
    //     LoginManager.logInWithReadPermissions(['public_profile']).then(
    //         function(result) {
    //             if (result.isCancelled) {
    //                 alert('Login was cancelled');
    //             } else {
    //                 alert('Login was successful with permissions: '
    //                     + result.grantedPermissions.toString());
    //             }
    //         },
    //         function(error) {
    //             alert('Login failed with error: ' + error);
    //         }
    //     );
    // }

    getUser(response) {
        // console.log('getUser');
        this.props.userAction.authLinkedIn(response.access_token);

    }

    async authFacebook() {
        const {type, token} = await Expo.Facebook.logInWithReadPermissionsAsync('254728085376710', {
            permissions: ['public_profile'],
        });
        if (type === 'success') {
            // Get the user's name using Facebook's Graph API
            const response = await fetch(
                `https://graph.facebook.com/me?access_token=${token}`);
            const data = await response.json();
            alert(
                `Logged in!, Hi ${data.name}!`
            );
            await this.setState({facebookName: data.name});
            console.log('this is facebook name', this.state.facebookName);
        }
    }

    instagramLogin() {
        this.props.userAction.authInstagram()
            .then(() => {
                console.log('this is redirect to action instagram login')
            })
    }

    logout() {
        console.log('this is logout');
        Cookie.clear().then(() => {
            this.setState({isntaToken: ''})
        })
    }

    getTokenModal() {

    }

    pushNotificationTest2() {
        const message = 'this is test push notification with server';
        console.log(message);
        this.props.userAction.sendPushNotification2(message)
            .then(() => {
                console.log('this is test push notification with server');
            })
            .catch(console.error.bind(console))
        // console.log('this is token',token);
    }

    redirectToCharts() {
        const {navigate} = this.props.navigation;
        navigate('ExpoTab');
    }

    netsAuthorization() {
        Animated.timing(
            this._animValue2,
            {
                toValue: 1,
                duration: 2000,
                easing: Easing.bounce,
                useNativeDriver: true
            }
        ).start();
        this.setState({disAnimate: false})
    }

    authPanelDisAnimate() {
        console.log('Test press');

        Animated.timing(
            this._animValue2,
            {
                toValue: 0,
                duration: 2000,
                easing: Easing.bounce,
                useNativeDriver: true
            }
        ).start();
        this.setState({disAnimate: true})
    }

    handlePressIn(){
        Animated.spring(this.animatedValue1,{
          toValue:0.5
        }).start()
    }
    handlePressOut(){
        Animated.spring(this.animatedValue1,{
            duration:2000,
            toValue:1,
            friction:4,
            tension:40
        }).start()
    }

    // readText(){
    //     /**
    //      * @param {string} imgPath - The path of the image.
    //      * @param {string} lang - The language you want to process.
    //      * @param {object} tessOptions - Tesseract options.
    //      */
    //     const tessOptions = {
    //         whitelist: null,
    //         blacklist: '1234567890\'!"#$%&/()={}[]+*-_:;<>'
    //     };
    //     RNTesseractOcr.recognize('../images/123.png', 'LANG_RUSSIAN', tessOptions)
    //         .then((result) => {
    //             this.setState({ ocrResult: result });
    //             console.log("OCR Result: ", result);
    //         })
    //         .catch((err) => {
    //             console.log("OCR Error: ", err);
    //         })
    //         .done();
    // }
    readText(){
    console.log('this is from text recognizer');
            try {
                const options = {
                    quality: 0.8,
                    base64: true,
                    skipProcessing: true,
                };
                const { uri } =  '../images/123.png';
                const visionResp =  RNTextDetector.detectFromUri(uri);
                // const visionResp =  '../images/123.png';
                console.log('visionResp', visionResp);
            } catch (e) {
                console.warn(e);
            }


    }

    render() {

        const {toScroll} = this.state;
        const inputAccessoryViewID = "uniqueID";

        const spin = this.spinValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '360deg']
        })

        const slide2 = this._animValue2.interpolate({
            inputRange: [0, 1],
            outputRange: [-500, 0],


        })
        const animatedStyle = {transform: [{scale:this.animatedValue1}]}
        return (
            //<View style={mainContainer(this.state.heightOfElementWhenKeyboardOn).container}>
            <NotificationWrapper>
                <KeyboardAvoidingView enabled behavior="padding">
                    <ScrollView
                        //style={mainContainer(this.state.heightOfElementWhenKeyboardOn).container}
                    >
                        <TouchableWithoutFeedback onPressOut={this.authPanelDisAnimate.bind(this)}>
                            <View>
                                <View>
                                    {/*<ScrollView ref='scrollView' {...this.scrollviewProps}>*/}
                                    {/*<View >*/}
                                    <View style={Container.container}>
                                        <FormLabel>Name</FormLabel>
                                        <FormInput onEndEditing={this.checkName.bind(this)}
                                            // onChangeText={(text) => {this.setState({text});
                                            // console.log('this is the text from input',this.state.text)} }

                                            // onChangeText={this.onChange}
                                            // ref={'email'}
                                            // ref={ref=>this.emailInput=ref}
                                        />

                                        <FormLabel>Name</FormLabel>
                                        <FormInput onEndEditing={this.checkName.bind(this)}/>
                                        <FormLabel>Name</FormLabel>
                                        <FormInput accessible={true} onEndEditing={this.checkName.bind(this)}/>

                                        <Button title="push notification"
                                                onPress={this.pushNotificationTest2.bind(this)}/>

                                        <TouchableOpacity onPress={this.redirectToCharts.bind(this)}><Image
                                            source={require('../images/pinterest.png')}
                                            accessible={true}
                                            accessibilityLabel="Tap me!"
                                            style={{display: 'flex', justifyContent: 'center', width: 50, height: 50}}
                                        />
                                        </TouchableOpacity>


                                        {/*https://bitbucket.org/coleman333/reactnodetemplate.git*/}

                                        {/*<TouchableOpacity onPress={this.state.disAnimate ? this.netsAuthorization.bind(this) : this.authPanelDisAnimate.bind(this)}>*/}
                                        {/*<Image source={require('../images/Facebook.png')}*/}
                                        {/*style={{display:'flex', justifyContent:'center',  width: 50, height: 50}}*/}
                                        {/*/>*/}
                                        {/*</TouchableOpacity>*/}

                                        {/*<Button title={'linckedIn'} onPress={this.authLinkedIn.bind(this)}></Button>*/}

                                        {/*<View style={st.container}>*/}
                                        {/*<Text style={st.label}>Welcome to the Facebook SDK for React Native!</Text>*/}
                                        {/*<LoginButton*/}
                                        {/*publishPermissions={["email"]}*/}
                                        {/*onLoginFinished={*/}
                                        {/*(error, result) => {*/}
                                        {/*if (error) {*/}
                                        {/*alert("Login failed with error: " + error.message);*/}
                                        {/*} else if (result.isCancelled) {*/}
                                        {/*alert("Login was cancelled");*/}
                                        {/*} else {*/}
                                        {/*alert("Login was successful with permissions: " + result.grantedPermissions)*/}
                                        {/*}*/}
                                        {/*}*/}
                                        {/*}*/}
                                        {/*onLogoutFinished={() => alert("User logged out")}/>*/}
                                        {/*</View>*/}

                                        <View style={Container.container}>
                                            <View>
                                                <Animated.Image
                                                    style={{transform: [{rotateY: spin}], width: 200, height: 200}}
                                                    source={require('../images/SmartITLogo.png')}
                                                />
                                            </View>
                                            <TouchableOpacity
                                                onPress={this.state.disAnimate ? this.netsAuthorization.bind(this) : this.authPanelDisAnimate.bind(this)}
                                                style={{
                                                    borderWidth: 1,
                                                    // borderColor:'rgba(0,0,0,0.2)',
                                                    borderColor: 'black',
                                                    alignItems: 'center',
                                                    justifyContent: 'center',
                                                    width: 200,
                                                    height: 50,
                                                    backgroundColor: 'gray',
                                                    borderRadius: 50,

                                                }}
                                            ><Text>Авторизация</Text>
                                            </TouchableOpacity>

                                        </View>
                                    </View>

                                    {/*<View style={{height:this.state.heightOfElementWhenKeyboardOn}}>*/}

                                    {/*<ScrollView ref={this.myRef} scrollEnabled={true}>*/}
                                    {/*<Button onPress={this.onScrollTo.bind(this)} title="Reset Text" />*/}

                                    {/*<TouchableOpacity style={{width:50, height:50}} onPress={this.logout.bind(this)}>*/}
                                    {/*<Text style={{color: 'white'}}>logout</Text>*/}
                                    {/*</TouchableOpacity>*/}

                                    <View>
                                        <InstagramLogin
                                            ref='instagramLogin'
                                            clientId='f618d75e427548d9ac1d1e488ae7c55f'
                                            scopes={['public_content', 'follower_list']}
                                            onLoginSuccess={(token) => {
                                                this.setState({isntaToken: token});
                                            }}
                                            redirectUrl={'https://localhost:4433/api/auth/instagram/callback'}
                                            onLoginFailure={(data) => console.log(data)}
                                        />
                                    </View>

                                    <View>
                                        <LinkedInModal
                                            ref={ref => {
                                                this.modal = ref
                                            }}
                                            linkText={false}
                                            clientID={'86kf6lxq1fg5jq'}
                                            clientSecret={'S09ixHzSX2F5rRLO'}
                                            redirectUri="https://localhost:4433/"
                                            onSuccess={data => this.getUser(data)}
                                        />
                                        {/*<Button title="Open from external" onPress={() => this.modal.open()} />*/}
                                    </View>
                                    <Modal
                                        animationType="fade"
                                        presentationStyle="formSheet"
                                        transparent={false}
                                        visible={this.state.modalVisible}

                                        onRequestClose={() => {
                                            alert('Modal has been closed.');
                                        }}>
                                        <View style={{marginTop: 22}}>
                                            <View>
                                                <Text>Hello World!</Text>
                                                <Button title={'request for token'}
                                                        onPress={() => {
                                                            this.getTokenModal.bind(this);
                                                        }}></Button>
                                                <Button title={'Hide Modal'}
                                                        onPress={() => {
                                                            this.setModalVisible(!this.state.modalVisible);
                                                        }}>
                                                </Button>
                                            </View>
                                        </View>
                                    </Modal>
                                    <Animated.View style={{transform: [{translateX: slide2}], height: 55}}>
                                        <View style={{display: 'flex', justifyContent: 'center', flexDirection: 'row'}}>
                                            <TouchableOpacity onPress={this.authorization.bind(this)}>
                                                {/*<Image source={require('../images/qrCodeLogo.png')}*/}
                                                {/*style={{display:'flex', justifyContent:'center',  width: 50, height: 50}} />*/}
                                                <Icon name="qrcode" size={60} color="#424242" style={{marginRight: 2}}/>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={this.authFacebook.bind(this)}>
                                                {/*<Image*/}
                                                {/*source={require('../images/Facebook.png')}*/}
                                                {/*style={{display:'flex', justifyContent:'center',  width: 50, height: 50}}*/}
                                                {/*/>*/}
                                                <Icon name="facebook-square" size={60} color="#424242"
                                                      style={{marginRight: 2}}/>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => this.modal.open()}>
                                                {/*<Image*/}
                                                {/*source={require('../images/3-2-linkedin-download-png-thumb.png')}*/}
                                                {/*style={{display:'flex', justifyContent:'center',  width: 50, height: 50}}*/}
                                                {/*/>*/}
                                                <Icon name="linkedin-square" size={60} color="#424242"
                                                      style={{marginRight: 2}}/>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => this.refs.instagramLogin.show()}>
                                                {/*<Image*/}
                                                {/*source={require('../images/instagram.jpg')}*/}
                                                {/*style={{display:'flex', justifyContent:'center',  width: 50, height: 50}}*/}
                                                {/*/>*/}
                                                <Icon name="instagram" size={60} color="#424242"
                                                      style={{marginRight: 2}}/>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={this.authTwitter.bind(this)}>
                                                {/*<Image source={require('../images/twitter1.jpeg')}*/}
                                                {/*style={{display:'flex', justifyContent:'center',  width: 50, height: 50}}*/}
                                                {/*/>*/}
                                                <Icon name="twitter-square" size={60} color="#424242"
                                                      style={{marginRight: 2}}/>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={this.authGoogle.bind(this)}>
                                                {/*<Image*/}
                                                {/*source={require('../images/googlePlus.png')}*/}
                                                {/*style={{display:'flex', justifyContent:'center',  width: 50, height: 50}}*/}
                                                {/*/>*/}
                                                <Icon name="google-plus-square" size={60} color="#424242"
                                                      style={{marginRight: 2}}/>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={this.authPinterest.bind(this)}>
                                                {/*<Image*/}
                                                {/*source={require('../images/pinterest.png')}*/}
                                                {/*style={{display:'flex', justifyContent:'center',  width: 50, height: 50}}*/}
                                                {/*/>*/}
                                                <Icon name="pinterest-square" size={60} color="#424242"
                                                      style={{marginRight: 2}}/>
                                            </TouchableOpacity>
                                        </View>
                                    </Animated.View>

                                    {/*<View style={{display:'flex',justifyContent:'flex-end',flexDirection:'row',borderRadius:50, width:50,*/}
                                    {/*border:'solid black 1px'}}>*/}
                                    {/*<TouchableOpacity onPress={this.authFacebook.bind(this)}><Image*/}
                                    {/*source={require('../images/Facebook.png')}*/}
                                    {/*style={{display:'flex', justifyContent:'center',  width: 50, height: 50}}*/}
                                    {/*/>*/}
                                    {/*</TouchableOpacity>*/}
                                    {/*<Button title={'example'} onPress={this.animateNets.bind(this)}></Button>*/}

                                    {/*</View>*/}

                                    {/*<Button title={'Show Modal'} style={{width:100}}*/}
                                    {/*onPress={() => {*/}
                                    {/*this.setModalVisible(true);*/}
                                    {/*}}>*/}
                                    {/*</Button>*/}

                                    {/*TouchableWithoutFeedback*/}

                                    <View style={animButtonStyle.container}>
                                        <TouchableWithoutFeedback onPressIn={this.handlePressIn.bind(this)}
                                                                  onPressOut={this.handlePressOut.bind(this)}
                                                                  onPress={this.readText.bind(this)}>
                                        <Animated.View style={[animButtonStyle.buttonStyle,animatedStyle]}>
                                            <Icon name="pinterest-square" size={60} color="#424242"/>
                                        </Animated.View>
                                        </TouchableWithoutFeedback>
                                    </View>
                                </View>

                            </View>
                        </TouchableWithoutFeedback>
                    </ScrollView>
                </KeyboardAvoidingView>
            </NotificationWrapper>
        );
    }
}

const animButtonStyle = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    buttonStyle:{
        height:50,
        width:100,
        backgroundColor:'black',
        color:'white'
    }
})

const mainContainer = (height) => StyleSheet.create({
    container: {
        height: height, //

    },
});

const st = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    label: {
        fontSize: 16,
        fontWeight: 'normal',
        marginBottom: 48,
    },
});

const subMainContainer = StyleSheet.create({
    container: {
        // flex:1,
        // display: 'flex',
        width: "100%",
        height: '100%',
        backgroundColor: 'white',
        overflow: 'scroll',
        // alignItems: 'center',
        // justifyContent: 'center'
    },
});
const Container = StyleSheet.create({
    container: {
        flex: 1,
        display: 'flex',
        width: "100%",
        height: '100%',
        // backgroundColor: 'white',
        overflow: 'scroll',
        alignItems: 'center',
        justifyContent: 'space-around'
    },
});

const authButtonStyle = StyleSheet.create({
    container: {
        backgroundColor: 'green',
        borderRadius: 25,
        width: 100,
        height: 40
    }
});
const warningStyle = StyleSheet.create({
    container: {
        backgroundColor: '#f2eee2',
        borderRadius: 5,
        // width: 100,
        height: 40
    }
});

const mapStateToProps = (state) => {
    console.log(state.dimension);
    return {
        users: state.userReducer.allUsers,
        // gists: state.userReducer.gists
        user: state.userReducer.login,

    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        userAction: bindActionCreators(userAction, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MainMenu);
