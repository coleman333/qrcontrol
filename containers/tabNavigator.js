import React from 'react';
import MenuToScanGood from './MenuToScanGood';
import MainMenu from "./MainMenu";
import { createBottomTabNavigator } from 'react-navigation';
import { Text, View } from 'react-native';
import { Ionicons } from '@expo/vector-icons'; // 6.2.2
import { TabNavigator, TabBarBottom } from 'react-navigation'; // 1.0.0-beta.27
import PersonalCabinetStatistic from "./PersonalCabinetStatistic";
// import Navigator from './Navigator'
const myTabNavigator = createBottomTabNavigator(
    {
        MainMenu: MainMenu,
        PersonalCabinetStatistic: PersonalCabinetStatistic, //вот здесь должна открыться эта страница и работать этот навигатор
    },
    {
        navigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, tintColor }) => {  //фокус не удался
                const { routeName } = navigation.state;
                let iconName;
                if (routeName === 'MainMenu') {
                    iconName = `ios-information-circle${focused ? '' : '-outline'}`;
                } else if (routeName === 'PersonalCabinetStatistic') {
                    iconName = `ios-options${focused ? '' : '-outline'}`;
                }

                // You can return any component that you like here! We usually use an
                // icon component from react-native-vector-icons
                return <Ionicons name={iconName} size={25} color={tintColor} />;
            },
        }),
        tabBarOptions: {
            activeTintColor: 'tomato',
            inactiveTintColor: 'gray',
        },
        // tabBarComponent: TabBarBottom,
        tabBarPosition: 'bottom',

        animationEnabled: false,
        swipeEnabled: false,
    }
);
export default myTabNavigator;
