import React from 'react';
import { createStackNavigator,} from 'react-navigation';
import QrScanner from './QrScanner';
import MenuToScanGood from './MenuToScanGood';
import MainMenu from "./MainMenu";
import QrScannerForGoods from "./QrScannerForGoods";
import PersonalCabinetStatistic from "./PersonalCabinetStatistic";
import myTabNavigator from "./tabNavigator";
import ExpoTab from "./ExpoTab";
import {Button} from 'react-native';


import { createBottomTabNavigator } from 'react-navigation';
import { Text, View } from 'react-native';
import { Ionicons } from '@expo/vector-icons'; // 6.2.2
// import { TabNavigator, TabBarBottom } from 'react-navigation'; // 1.0.0-beta.27
import TabNavigator from './tabNavigator';

// const Navigator = createBottomTabNavigator(
//     {
//         MainMenu: MainMenu,
//         MenuToScanGood: MenuToScanGood,
//     },
//     {
//         navigationOptions: ({ navigation }) => ({
//             tabBarIcon: ({ focused, tintColor }) => {
//                 const { routeName } = navigation.state;
//                 let iconName;
//                 if (routeName === 'MainMenu') {
//                     iconName = `ios-information-circle${focused ? '' : '-outline'}`;
//                 } else if (routeName === 'MenuToScanGood') {
//                     iconName = `ios-options${focused ? '' : '-outline'}`;
//                 }
//
//                 // You can return any component that you like here! We usually use an
//                 // icon component from react-native-vector-icons
//                 return <Ionicons name={iconName} size={25} color={tintColor} />;
//             },
//         }),
//         tabBarOptions: {
//             activeTintColor: 'tomato',
//             inactiveTintColor: 'gray',
//         },
//         // tabBarComponent: TabBarBottom,
//         tabBarPosition: 'bottom',
//
//         animationEnabled: false,
//         swipeEnabled: false,
//     }
// );


const Navigator = createStackNavigator({
    QrScanner: { screen: QrScanner,
        navigationOptions:({ navigation }) =>({
            headerLeft:null,
            header: null,
            headerRight:(
                <Button style={{marginRight:10}}
                    onPress={() =>navigation.navigate('menuToScanGood')}
                    title="Info"
                    //color="#fff"
                />
            )
        })
    },
    MenuToScanGood: {screen: MenuToScanGood,     ///вот здесь переход на др навигатор и на др страницу
        navigationOptions:({navigation})=>({
            header:null
        })
    },

    // myTabNavigator: {screen: myTabNavigator,
    //     navigationOptions:({navigation})=>({
    //         header:null
    //     })
    // },
    ExpoTab: {screen: ExpoTab,
        navigationOptions:({navigation})=>({
            header:null
        })
    },
    PersonalCabinetStatistic: {screen: PersonalCabinetStatistic,
        navigationOptions:({navigation})=>({
            header:null
        })
    },

    MainMenu: {screen: MainMenu,
        navigationOptions:({ navigation }) =>({
            headerLeft: null,
            header: null
            //     title: 'Home',
            //     headerStyle: {
            //         backgroundColor: 'gray',
            //
            //     },
            //     headerTintColor: '#fff',
            //     headerTitleStyle: {
            //         marginLeft:'45%',
            //         // display:'flex',
            //         // flex:1,
            //         // flexDirection:'row',
            //         // justifyContent:'center',
            //         // alignItems:'center',
            //         fontWeight: 'bold',
            //     },
        })
    },
    QrScannerForGoods: {screen: QrScannerForGoods}

}, {initialRouteName: 'MainMenu'});

export default Navigator;
