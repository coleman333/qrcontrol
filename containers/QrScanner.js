import React, { Component } from 'react';
import { Alert, Linking, Dimensions, LayoutAnimation, Text, View, StatusBar, StyleSheet,
    TouchableOpacity,} from 'react-native';
import { BarCodeScanner, Permissions } from 'expo';
import {bindActionCreators} from "redux";
import userAction from "../actions/userAction";
import connect from "react-redux/es/connect/connect";
import { StackActions, NavigationActions } from 'react-navigation';

class QrScanner extends Component {
    state = {
        hasCameraPermission: null,
        lastScannedUrl: null,
    };

    componentDidMount() {
        this._requestCameraPermission();

    }

    _requestCameraPermission = async () => {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        this.setState({
            hasCameraPermission: status === 'granted',
        });
    };

    _handleBarCodeRead = result => {
        try {
            // const {page2}=this.props.navigation.state.params();
            // console.log('this is page with params' , page2);
            // const {page}=this.props.navigation.getParam();
            // console.log("this is the page name",page ,"-----------",this.props.navigation.getParam);
            // alert(`hello ${name} ${age}`)
            // if(page === 'ProductScanner'){
            //     this.props.navigation.navigate('MainMenu');
            // }
            // else {
            console.log('before action login ', result.data, typeof result.data);
            const user = result.data;
            this.props.userAction.login(user)
                .then(() => {
                    if (`${this.props.user.name} ${this.props.user.phone}` === result.data) {
                           const resetAction = StackActions.reset({
                            index: 0,
                            actions: [
                                NavigationActions.navigate({ routeName: 'menuToScanGood'},{isAdmin:this.props.user.role}),
                            ],
                        });
                        this.props.navigation.dispatch(resetAction);
                    }
                    else {
                        this.props.navigation.navigate('MainMenu',{page:'unAuthorized'});
                    }
                })
                .catch((error) => {
                    console.log("Api call error", error);
                    alert(error.message);
                });

            if (result.data !== this.state.lastScannedUrl) {
                LayoutAnimation.spring();
                this.setState({lastScannedUrl: result.data});

            }
        }catch (ex) {
            console.log('Error=>>',ex);
        }
        // }
    };

    render() {
        return (
            <View style={styles.container}>

                {this.state.hasCameraPermission === null
                    ? <Text>Requesting for camera permission</Text>
                    : this.state.hasCameraPermission === false
                        ? <Text style={{ color: '#fff' }}>
                            Camera permission is not granted
                        </Text>
                        : <BarCodeScanner
                            onBarCodeRead={this._handleBarCodeRead}
                            style={{
                                height: Dimensions.get('window').height,
                                width: Dimensions.get('window').width,
                            }}
                        />}

                {this._maybeRenderUrl()}

                <StatusBar hidden />
            </View>
        );
    }

    _handlePressUrl = () => {
        Alert.alert(
            'Open this URL?',
            this.state.lastScannedUrl,
            [
                {
                    text: 'Yes',
                    onPress: () => Linking.openURL(this.state.lastScannedUrl),
                },
                { text: 'No', onPress: () => {} },
            ],
            { cancellable: false }
        );
    };

    _handlePressCancel = () => {
        this.setState({ lastScannedUrl: null });
    };

    _maybeRenderUrl = () => {
        if (!this.state.lastScannedUrl) {
            return;
        }

        return (
            <View style={styles.bottomBar}>
                <TouchableOpacity style={styles.url} onPress={this._handlePressUrl}>
                    <Text numberOfLines={1} style={styles.urlText}>
                        {this.state.lastScannedUrl}
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.cancelButton}
                    onPress={this._handlePressCancel}>
                    <Text style={styles.cancelButtonText}>
                        Cancel
                    </Text>
                </TouchableOpacity>
            </View>
        );
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#000',
    },
    bottomBar: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: 'rgba(0,0,0,0.5)',
        padding: 15,
        flexDirection: 'row',
    },
    url: {
        flex: 1,
    },
    urlText: {
        color: '#fff',
        fontSize: 20,
    },
    cancelButton: {
        marginLeft: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    cancelButtonText: {
        color: 'rgba(255,255,255,0.8)',
        fontSize: 18,
    },
});

const mapStateToProps = (state) => {
    return {
        // users: state.userReducer.allUsers,
        // gists: state.userReducer.gists
        user: state.userReducer.user
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        userAction: bindActionCreators(userAction, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(QrScanner);

