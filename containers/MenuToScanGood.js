import React from 'react';
import {    StyleSheet, Text, Button, View, TextInput, TouchableOpacity, ScrollView, Keyboard, Dimensions} from 'react-native';
import { connect } from "react-redux";
import userAction from "../actions/userAction";
import {bindActionCreators} from "redux";
import ModalAfterAuth from '../containers/ModalAfterAuth'
import { FormLabel, FormInput, FormValidationMessage } from 'react-native-elements'
import _ from 'lodash';

class MenuToScanGood extends React.Component {

    state={
        heightOfElementWhenKeyboardOn:'100%',
        newUserName: "",

    };

    toScanGoods(){
        // const { navigate } = this.props.navigation;
        // navigate('QrScannerForGoods',{page: 'ProductScanner'});
        // navigate('QrScanner',{page:'ProductScanner'} );
        console.log('this is new user ',this.state.newUserName , this.textInput.current.value)

    }

    componentWillUnmount () {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow (e) {
        this.setState({ toScroll: true });
        this.setState({shortHeight: Dimensions.get('window').height - e.endCoordinates.height});
        this.setState({normalHeight: Dimensions.get('window').height});
        this.setState({keyboardHeight: e.endCoordinates.height});
        this.setState({heightOfElementWhenKeyboardOn: this.state.shortHeight});
    }

    _keyboardDidHide () {
        this.setState({ toScroll: false });
        this.setState({ heightOfElementWhenKeyboardOn: this.state.normalHeight });

    }


    checkName(ev){
        this.props.userAction.checkName()
        console.log('this is new name value',ev.nativeEvent.text)
    }

    checkPhone(){

    }

    checkEmail(){

    }

    componentDidMount(){
        const b = Dimensions.get('screen');
        console.log(b);

        Dimensions.addEventListener('change', () => {
            const a = Dimensions.get('screen');
            console.log(a);
        });

        setTimeout(() => {
            const {user} = this.props;
            {/*<ModalAfterAuth/>*/}
            alert(`Вы зарегистрированы как : 
            ${user.name}
            Ваш тел: 
             ${user.phone}
            `);
        }, 1000);
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
// vasiliy petrov ivanov
// 0938888888
    }

    render() {
        // const {name,age}=this.props.navigation.state.params;
        // let role='';
        // console.log(this.props.navigation.state, {depth: 5});
        // if(this.props.navigation.state.params) {
        //    console.log(this.props.navigation.state.params, {depth: 5});
        //    let {role} = this.props.navigation.state.params;
        //    this.setState({role});
        //    // this.setState({role: this.props.navigation.state.params.role})
        // }


        // inputStyle={{width:'100%'}}
            // alert(`hello ${name} ${age}`)
        // ref={this.myRef}
            return (
                <View >
                    {this.props.user.role === 'admin'&& <ScrollView style={mainContainer(this.state.heightOfElementWhenKeyboardOn).container}>
                        <View style={Container.container}>
                            <FormLabel>Name</FormLabel>
                            <FormInput
                                // onBlur={this.checkName.bind(this)}
                                onEndEditing={this.checkName.bind(this)}
                                // onChangeText={(a)=>console.log('onChangeText',a)}
                            />
                            {/*<FormValidationMessage>Error message</FormValidationMessage>*/}
                            <FormLabel>phone</FormLabel>
                            <FormInput onBlur={this.checkPhone.bind(this)}/>
                            {/*<FormValidationMessage>Error message</FormValidationMessage>*/}
                            <FormLabel>email</FormLabel>
                            <FormInput onBlur={this.checkEmail.bind(this)}/>
                            {/*<FormValidationMessage>Error message</FormValidationMessage>*/}

                            <Text>Для сканирования продукта нажмите кнопку</Text>
                            <TouchableOpacity onPress={this.toScanGoods.bind(this)} style={{
                            borderWidth: 1,
                            // borderColor:'rgba(0,0,0,0.2)',
                            borderColor: 'black',
                            alignItems: 'center',
                            justifyContent: 'center',
                            width: 200,
                            height: 50,
                            backgroundColor: 'gray',
                            borderRadius: 50,
                            }}
                            ><Text>Scan the product</Text>
                            </TouchableOpacity>
                        </View>
                        {/*<View style={Label.container} >*/}
                            {/*<Text>Для сканирования продукта нажмите кнопку</Text>*/}
                            {/*<TouchableOpacity onPress={this.toScanGoods.bind(this)} style={{*/}
                                {/*borderWidth: 1,*/}
                                {/*// borderColor:'rgba(0,0,0,0.2)',*/}
                                {/*borderColor: 'black',*/}
                                {/*alignItems: 'center',*/}
                                {/*justifyContent: 'center',*/}
                                {/*width: 200,*/}
                                {/*height: 50,*/}
                                {/*backgroundColor: 'gray',*/}
                                {/*borderRadius: 50,*/}
                            {/*}}*/}
                            {/*><Text>Scan the product</Text>*/}
                            {/*</TouchableOpacity>*/}
                        {/*</View>*/}
                    </ScrollView>}
                    {/*{this.props.user.role === 'guest' &&<View style={{height:this.state.keyboardSpace}}>*/}
                        {/*<View><Text>Для сканирования продукта нажмите кнопку</Text></View>*/}
                        {/*<TouchableOpacity onPress={this.toScanGoods.bind(this)} style={{*/}
                            {/*borderWidth: 1,*/}
                            {/*// borderColor:'rgba(0,0,0,0.2)',*/}
                            {/*borderColor: 'black',*/}
                            {/*alignItems: 'center',*/}
                            {/*justifyContent: 'center',*/}
                            {/*width: 200,*/}
                            {/*height: 50,*/}
                            {/*backgroundColor: 'gray',*/}
                            {/*borderRadius: 50,*/}
                        {/*}}*/}
                        {/*><Text>Scan the product</Text>*/}
                        {/*</TouchableOpacity>*/}
                    {/*</View>}*/}
                </View>

            );

    }
}
const mainContainer = (height) => StyleSheet.create({
    container: {
        height: height,
    },
});
const FormInputStyle = StyleSheet.create({
    container: {
        // height: 900
        // height: '100%'
    }
});

const Container = StyleSheet.create({
    container: {
        flex:1,
        display: 'flex',
        width: "100%",
        height: '100%',
        // backgroundColor: 'white',
        overflow:'scroll',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop:'30%'
    },
});

const InnerContainer2 = StyleSheet.create({
    container: {
        flex:1,
        display: 'flex',
        width: "100%",
        height: '100%',
        backgroundColor: 'white',
        overflow:'scroll',
        alignItems: 'center',
        justifyContent: 'center'
    },
});
const InnerContainer1 = StyleSheet.create({
    container: {
        flex:1,
        display: 'flex',
        // width: "100%",
        // height: '100%',
        backgroundColor: 'white',
        overflow:'scroll',
        // alignItems: 'center',
        justifyContent: 'center'
    },
});
const Form = StyleSheet.create({
    container: {
        flex:3,
        display: 'flex',
        // width: "100%",
        // height: '100%',
        backgroundColor: 'white',
        overflow:'scroll',
        // alignItems: 'center',
        justifyContent: 'center'
    },
});
const Label = StyleSheet.create({
    container: {
        flex:1,

        display: 'flex',
        // width: "100%",
        // height: '100%',
        backgroundColor: 'white',
        overflow:'scroll',
        alignItems: 'center',
        justifyContent: 'center'
    },
});



const mapStateToProps = (state) => {
    return {
        // users: state.userReducer.allUsers,
        // gists: state.userReducer.gists
        user: state.userReducer.user

    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        userAction: bindActionCreators(userAction, dispatch),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MenuToScanGood);
