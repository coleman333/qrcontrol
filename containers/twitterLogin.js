import React from 'react-native';
var { NativeModules } = React;
const { TwitterSignin } = NativeModules;
class TwitterButton extends SocialButton {
    _twitterSignIn() {
        TwitterSignin.logIn({TWITTER_COMSUMER_KEY}, {TWITTER_CONSUMER_SECRET})
            .then((data) => {
                //use data
            }).catch((err) => {
            //handle err
        });
    }
    render() {
        return (
            <Icon name='social-twitter' size={32} color='white' style={styles.icon} onPress={this._twitterSignIn.bind(this)}/>
        );
    }
};