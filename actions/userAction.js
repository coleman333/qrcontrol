import { createActionAsync } from "redux-act-async";
import axios from "axios";
// import { createAction } from "redux-actions";


export default {
    getTestUsers: createActionAsync('GET_TEST_USERS', () => {
       return axios({
            method: 'get',
            url: `https://jsonplaceholder.typicode.com/users`,
        });
    }, {rethrow: true}),
    getGists: createActionAsync('GET_GISTS', () => {
        return axios({
            method: 'get',
            url: `https://api.github.com/gists`,
        });
    }, {rethrow: true}),

    login: createActionAsync('LOGIN', (user) => {
        return axios({
            method: 'post',
             url: `http://192.168.0.201:3003/api/users/login`,
             // url: `http://192.168.0.201:4433/api/users/login`,
            data:  {user},
        });
    }, {rethrow: true}),
    checkName: createActionAsync('CHECK_NAME', (name) => {
        return axios({
            method: 'post',
             url: `http://192.168.0.111:4433/api/users/check-name`,
            data:  {name},
        });
    }, {rethrow: true}),

    productScan: createActionAsync('PRODUCT', (product) => {
        return axios({
            method: 'get',
            url: `http://192.168.0.109:4433/api/users/product`,product
            // data: { user},
        });
    }, {rethrow: true}),

    sendPushNotification: createActionAsync('SEND_PUSH_NOTIFICATION', (message) => {
        console.log(message , 'this is action side');
        return axios({
            method: 'post',
            url: `http://192.168.0.201:3003/api/users/send-push-notifications`,//это работает
            data: { message},
        });
    }, {rethrow: true}),
    sendPushNotification2: createActionAsync('SEND_PUSH_NOTIFICATION2', (message) => {
        console.log(message , 'this is action side123');
        return axios({
            method: 'post',
            url: `https://192.168.0.201:4433/api/users/send-push-notifications2`, //этот даже не реагирует
            data: { message},
        }).catch(error => {
            console.log('___________');
            console.log(error);
            console.log('___________');
        });
    }, {rethrow: true}),


    authFB: createActionAsync('AUTH_FB', () => {
        console.log('this is facebook auth action')
        return axios({
            method: 'get',
            url:`https://localhost:4433/api/auth/facebook`
            // url: `https://api.linkedin.com/v1/people/~?format=json`
            // url:`https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=${'86kf6lxq1fg5jq'}&
            // redirect_uri=http://localhost:3006/&state=DCEeFWf45A53sdfKef424&scope=r_basicprofile`
            // data: { user},
        });
    }, {rethrow: true}),

    // authLinkedIn: createActionAsync('AUTH_LINKED_IN', (access_token) => {
    //     console.log('this is linked in auth action');
    //     return axios({
    //         headers: {
    //             'Content-Type': 'application/x-www-form-urlencoded'
    //         },
    //         method: 'post',
    //         url:`https://www.linkedin.com/oauth/v2/accessToken`,
    //         data: {
    //             grant_type: 'authorization_code',
    //             code: access_token,
    //             // redirect_uri: 'http://localhost:3006/',
    //             redirect_uri: 'https://github.com/',
    //             client_id: '86kf6lxq1fg5jq',
    //             client_secret: 'S09ixHzSX2F5rRLO'
    //         }
    //
    //
    //     }).then(data => {
    //         console.log('xxxx', data);
    //
    //     }).catch(err => {
    //         console.log('err', err)
    //     });
    // }, {rethrow: true}),

    authInstagram: createActionAsync('AUTH_INSTAGRAM',(acces_token)=>{
        return axios({
            method:'get',
            url:'https://localhost:4433/api/auth'
        });
    }, {rethrow: true}),

    authLinkedIn: createActionAsync('AUTH_LINKED_IN', (access_token) => {
        console.log('this is linked in auth action', access_token);
        return axios({
            headers: {
                'Authorization': `Bearer ${access_token}`
            },
            method: 'get',
            url:`https://api.linkedin.com/v1/people/~?format=json`,
            // data: {
            //     grant_type: 'authorization_code',
            //     code: access_token,
            //     redirect_uri: 'https://github.com/',
            //     client_id: '86kf6lxq1fg5jq',
            //     client_secret: 'S09ixHzSX2F5rRLO'
            // }


        }).then(({data}) => {
            console.log('xxxx', data);

        }).catch(err => {
            console.log('err', err)
        });
    }, {rethrow: true}),

    // https://api.linkedin.com/v1/people/~?format=json

};

