import { Permissions, Notifications } from 'expo';
import axios from 'axios';

// const PUSH_ENDPOINT = 'http://192.168.137.2:3003/users/push-token';
const PUSH_ENDPOINT = 'https://localhost:4433/users/push-token';

export async function registerForPushNotificationsAsync() {
    const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
    );
    let finalStatus = existingStatus;

    // only ask if permissions have not already been determined, because
    // iOS won't necessarily prompt the user a second time.
    if (existingStatus !== 'granted') {
        // Android remote notification permissions are granted during the app
        // install, so this will only ask on iOS
        const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
        finalStatus = status;
    }

    // Stop here if the user did not grant permissions
    if (finalStatus !== 'granted') {
        return;
    }
    // Get the token that uniquely identifies this device
    const token = await Notifications.getExpoPushTokenAsync();

        return alert(`Your token is: ${token}`);   //ExponentPushToken[AhcfyuOWnprDnvzkaue1qQ]
    // POST the token to your backend server from where you can retrieve it to send push notifications.
    return axios({
        method: 'POST',
        url: PUSH_ENDPOINT,
        data: {
            token: {
                value: token,
            },
        },
    });
}