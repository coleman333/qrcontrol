import cloneDeep from 'lodash/cloneDeep';
import userAction from '../actions/userAction';
import {createReducer} from 'redux-act';

const initialState = {
    processing: 0,
    error: {},
    user: {},
    allUsers: [],
    authLI:{},
    authFacebook:{}
};

export default createReducer({
    [userAction.getTestUsers.request]: (state, payload) => {
        const newState = cloneDeep(state);
        return newState;
    },
    [userAction.getTestUsers.ok]: (state, payload) => {
        const newState = cloneDeep(state);
        newState.allUsers = payload.response.data;
        // console.log('this is from react native', newState.allUsers);
        return newState;
    },
    [userAction.getTestUsers.error]: (state, payload) => {
        const newState = cloneDeep(state);

        return newState;
    },

    [userAction.login.request]: (state, payload) => {
        const newState = cloneDeep(state);
        return newState;
    },
    [userAction.login.ok]: (state, payload) => {
        const newState = cloneDeep(state);
        if(payload.response.data.user[0]){
            newState.user = payload.response.data.user[0];
        }
        else{
            newState.user ={}
        }
        console.log('LOGIN OK', newState.user );
        return newState;
    },
    [userAction.login.error]: (state, payload) => {
        const newState = cloneDeep(state);

        return newState;
    },

    [userAction.authLinkedIn.request]: (state, payload) => {
        const newState = cloneDeep(state);
        return newState;
    },
    [userAction.authLinkedIn.ok]: (state, payload) => {
        const newState = cloneDeep(state);
        newState.authLI = payload;

        console.log('LINKED_IN_OK_LINK', newState.authLI );
        return newState;
    },
    [userAction.authLinkedIn.error]: (state, payload) => {
        const newState = cloneDeep(state);

        return newState;
    },

    [userAction.authFB.request]: (state, payload) => {
        const newState = cloneDeep(state);
        return newState;
    },
    [userAction.authFB.ok]: (state, payload) => {
        const newState = cloneDeep(state);
        newState.authFacebook = payload;

        console.log('FACEBOOK OK REDUCER', newState.authLI );
        return newState;
    },
    [userAction.authFB.error]: (state, payload) => {
        const newState = cloneDeep(state);

        return newState;
    },

}, initialState);
